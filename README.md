# Larissa Jacob Pimenta Challenge

Technologies used:


 . Cucumber - Used to create BDD scripts
 
 . PageObjects - Unsed to write a cleaner code and encapsulate the elements of the page
 
 . Ruby - Used to create the automation scripts


# Challenge

### Project Structure

. /features/specs - Features in BDD

. /features/step_definitions - Step definitions of the features in BDD

. /support/Pages - Page Objects File

. /logs/shots - screenshot of the test evidences

### Install and Run
First let's run the web app to see how it's look like:
```
npm install -g parcel
```
```
npm install
```
```
npm start
```
Now you must be able to see the web app on http://localhost:3000

Now install ruby and run the following comands

```
gem install bundler
```
```
bundle install
```
```
cucumber
```