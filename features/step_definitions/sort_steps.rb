Given("The User wants to access the sort page") do
@sort.load

end

When("The user sort the itens from {int} to {int} by drag and drop") do |source, target|

  @source = source
  @target = target
end

Then("The user should see the list ordered") do

  @sort.sort(@source, @target)
  
end



  