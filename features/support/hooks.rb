Before do
    @sort = SortPage.new
end



After do |scenario|
    scenario_name = scenario.name
    scenario_name.tr('','_').downcase!
    screenshot = "logs/shots/#{scenario_name}.png"
    page.save_screenshot(screenshot)

end