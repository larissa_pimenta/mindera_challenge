class SortPage < SitePrism::Page
 
set_url 'http://localhost:3000/'


def sort (source, target)
    (source..target).each do |i|
        from = find('li', text: "Item #{i}")
        target = find("#app > ul > li:nth-child(#{i+1})")
        from.drag_to target
      end

end


end