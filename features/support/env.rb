require 'selenium-webdriver'
require 'capybara'
require 'capybara/cucumber'
require 'capybara/poltergeist'
require 'site_prism'


$env = ENV['BROWSER']
$headless = ENV['HEADLESS']

  if $headless
    Capybara.register_driver :selenium do |app|
      options = {
        :js_errors => false,
        :window_size => [1300,2000], #set whatever size you need here 
        :phantomjs_options => ['--ignore-ssl-errors=yes']
    }
    Capybara.javascript_driver = :poltergeist
    Capybara.current_driver = :poltergeist
    Capybara::Poltergeist::Driver.new(app, options)
        
    end
  end

  Capybara.configure do |config|
        if $env == 'chrome'
        config.default_driver = :selenium_chrome
        elsif $env == 'chrome_headless'
        config.default_driver = :selenium_chrome_headless
        end
    end

    Capybara.default_max_wait_time = 5